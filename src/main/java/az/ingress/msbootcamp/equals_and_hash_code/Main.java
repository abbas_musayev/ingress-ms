package az.ingress.msbootcamp.equals_and_hash_code;

import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;

@Slf4j
public class Main {

    public static void main(String[] args) {
        Customer customer = new Customer(1L,"Abbas","Musayev");
        Customer customer1 = new Customer(1L,"Abbas","Musayev");
        System.out.println("customer equals is customer1 = "+ customer1.equals(customer));
        System.out.println("customer == is customer1 = "+ (customer1 == customer));
        log.info("Customer {}",customer);

        Customer customer3 = new Customer(1L,"Abbas","Musayev");
        Customer customer4 = new CorporateCustomer(1L,"Abbas","Musayev","test");

        System.out.println("customer3 equals is customer4 = "+ customer3.equals(customer4));
        System.out.println("customer4 equals is customer3 = "+ customer4.equals(customer3));


        HashSet<Student> set = new HashSet<>();
        Student test = new Student(1, "Test");
        set.add(test);

        System.out.println("set have a student is = "+set.contains(new Student(1,"Test")));


    }
}

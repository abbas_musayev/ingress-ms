package az.ingress.msbootcamp.equals_and_hash_code;

import lombok.*;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Customer {

    private Long id;
    private String name;
    private String iban;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) && Objects.equals(name, customer.name) && Objects.equals(iban, customer.iban);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, iban);
    }
}

package az.ingress.msbootcamp.equals_and_hash_code;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorporateCustomer extends Customer{

    private String name;

    public CorporateCustomer(Long id, String name, String iban, String name1) {
        super(id, name, iban);
        this.name = name1;
    }

    public CorporateCustomer(String name) {
        this.name = name;
    }
}

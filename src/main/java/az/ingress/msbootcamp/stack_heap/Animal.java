package az.ingress.msbootcamp.stack_heap;

public class Animal {

    private static int ageStatic; // static primitive variables
    private static String nameStatic; // static reference variables;

    private int age;    //heap, instance primitive variables;
    private String name;    // heap, instance reference variables;

    public void bark(int volume, String message){
         // 1. local primitive variable, 2. local reference variable
    }

    static {
        System.out.println("Animal is loaded into JVM");
    }

    {
        System.out.println("Animal is created   ");
    }

}

package az.ingress.msbootcamp.stack_heap;

public class Main {

    public static void main(String[] args) {
        int a = 8;
        Animal b;
        System.out.println("Animal already Declared");
        b = new Animal();
        b = new Animal();
        b = new Animal();
        method1();
    }

    private static void method1(){
        System.out.println("Method 1 started");
        method2();
        System.out.println("Method 1 ended");
    }

    private static void method2(){
        System.out.println("Method 2 started");
        method3();
        System.out.println("Method 2 ended");
    }

    private static void method3(){
        System.out.println("Method 3 started");
        method4();
        System.out.println("Method 3 ended");
    }

    private static void method4(){
        System.out.println("Method 4 started");
        System.out.println("Method 4 ended");
    }
}

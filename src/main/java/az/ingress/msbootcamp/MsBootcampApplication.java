package az.ingress.msbootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsBootcampApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsBootcampApplication.class, args);
    }

}
